LPGBT library for Altium
David Porret - CERN - EP/ESE/ME

Release Notes:
23/10/2018 : first public release of the libraries. 
- One file contains the Altium schematic symbol and the other file contains a PCB footprint of the BGA package.
- All the ECLKx are the same but on the symbol ECLK28 has been placed with EDOUTEC and EDINEC as it is the most closest clock from those signals in the package.
- All the pins, when it was possible, have the ball-die delay defined in the library so in theory Altium is including this delay when matching the pcb tracks.
- Some swapping informations have been already defined for the ADC and the GPIO pins.
- PCB pads size and soldermask definition should be adjusted to match the PCB design rules of your PCB manufacturing company

17/4/2020: pinout for v2 ASIC
